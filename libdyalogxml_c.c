/** 
 * ----------------------------------------------------------------
 * $Id$
 * Copyright (C) 2003, 2004, 2005, 2006, 2007, 2009, 2010, 2011, 2014, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  \file  libdyalogxml_c.c 
 *  \brief Support for libdyalogxml.pl
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "libdyalog.h"
#include "builtins.h"

//#include <libxml/SAX.h>
//#include <libxml/xmlreader.h>

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>
#include <libxml/xmlreader.h>

static char *
XML_use_entities(char * s) 
{
    unsigned int l = 1+strlen(s);
    char * res = (char *) GC_MALLOC_ATOMIC(6*l);
    int in = l;
    int out = l;
    char * p = res;
    for(;*s ; s++,p++) {
        switch (*s) {
            case '&':
                sprintf(p,"&amp;");
                p += 4;
                break;
            case '<':
                sprintf(p,"&lt;");
                p += 3;
                break;
            case '>':
                sprintf(p,"&gt;");
                p += 3;
                break;
            default:
                *p = *s;
                break;
        }
    }
    *p=0;
    return res;
}


static
void XML_atom_print_aux( StmInf *stream, fol_t A)
{
    fol_t module = FOLSMB_MODULE(A);
    if (!FOLNILP(module)) {
        XML_atom_print_aux(stream,module);
        Stream_Putc(':',stream);
    }
    Stream_Puts(XML_use_entities(FOLSMB_NAME(A)),stream);    
}


Bool
XML_atom_print( int stm, sfol_t SA )
{
    fol_t A = SA->t;
    fkey_t Sk(A) = SA->k;
    Deref_And_Fail_Unless(A,FOLSMBP(A));
    XML_atom_print_aux(INT_TO_STM(stm),A);
    Succeed;
}

Bool
XML_string_print( int stm, sfol_t SA )
{
    fol_t A = SA->t;
    fkey_t Sk(A) = SA->k;
    Deref(A);
    if (FOLSMBP(A)) {
        XML_atom_print_aux(INT_TO_STM(stm),A);
    } else {
        dyalog_fprintf(INT_TO_STM(stm),"%&s",A,Sk(A));
    }
    Succeed;
}

extern char *newline_start;

static void
XML_Indent( StmInf *stream )
{
    Stream_Putc('\n',stream);
    Stream_Puts(newline_start,stream);
}

Bool
XML_print( int stm, xmlBufferPtr buffer ) 
{
//    dyalog_printf("XML print: I am here: '%s'\n",(const char *) buffer->content);
    StmInf *stream = (StmInf *)INT_TO_STM(stm);
    Stream_Puts((const char *) buffer->content, stream);
}


Bool
XML_start_element_print( int stm, sfol_t SElt, sfol_t SAttr, Bool empty)
{
    StmInf *stream = (StmInf *)INT_TO_STM(stm);
    fol_t Elt = SElt->t;
    fkey_t Sk(Elt) = SElt->k;
    fol_t Attr = SAttr->t;
    fkey_t Sk(Attr) = SAttr->k;
    Deref_And_Fail_Unless(Elt,FOLSMBP(Elt));
    XML_Indent(stream);
    Stream_Putc('<',stream);
    XML_atom_print_aux(stream,Elt);
    Deref(Attr);
    for(;FOLPAIRP(Attr);) {
        fol_t A=FOLPAIR_CAR(Attr);
        fkey_t Sk(A) = Sk(Attr);
        Deref(A);
        if (!FOLCMPP(A) && FOLCMP_ARITY(A) != 2)
            Fail;
        else {
            fol_t X=FOLCMP_REF(A,1);
            fkey_t Sk(X) = Sk(A);
            fol_t V=FOLCMP_REF(A,2);
            fkey_t Sk(V) = Sk(A);
            Deref(X);
            if (!FOLSMBP(X))
                Fail;
            Stream_Putc(' ',stream);
            XML_atom_print_aux(stream,X);
            Deref(V);
            if (FOLSMBP(V)) {
                char *s=XML_use_entities(FOLSMB_NAME(V));
                if (strchr(s,'"')) {
                    if (strchr(s,'\'')) {
                        unsigned long n = strlen(s);
                        char buff[10*n];
                        char *pin = s;
                        char *stop = pin+n+1;
                        char *pout = buff;
                        for(; pin < stop; pin++) {
                            if (*pin == '\'') {
                                *pout++ = '&';
                                *pout++ = 'a';
                                *pout++ = 'p';
                                *pout++ = 'o';
                                *pout++ = 's';
                                *pout++ = ';';
                            } else {
                                *pout++ = *pin;
                            }
                        }
                        *pout='\0';
                        dyalog_fprintf(stream,"=\'%s\'",buff);
                    } else {
                        dyalog_fprintf(stream,"=\'%s\'",s);
                    }
                } else {
                    dyalog_fprintf(stream,"=\"%s\"",s);
                }
            } else {
                dyalog_fprintf(stream,"=\"%&s\"",V,Sk(V)); 
            }
        }
        Attr = FOLPAIR_CDR(Attr);
        Deref(Attr);
    }
    if (empty)
        dyalog_fprintf(stream,"/");
    dyalog_fprintf(stream,">");
    Succeed;
}

Bool
XML_attribute_value_print( int stm, sfol_t SA, sfol_t SV )
{
    if (XML_atom_print(stm,SA)) {
        StmInf *stream = (StmInf *)INT_TO_STM(stm);
        fol_t V = SV->t;
        fkey_t Sk(V) = SV->k;
        Deref(V);
        if (FOLSMBP(V)) {
            char *s=XML_use_entities(FOLSMB_NAME(V));
            if (strchr(s,'"')) {
                dyalog_fprintf(stream,"=\'%s\'",s);
            } else {
                dyalog_fprintf(stream,"=\"%s\"",s);
            }
        } else {
            dyalog_fprintf(stream,"=\"%&s\"",V,Sk(V)); 
        }
        Succeed;
    } else {
        Fail;
    }
}


Bool
XML_close_element_print( int stm, sfol_t SElt)
{
    StmInf *stream = (StmInf *)INT_TO_STM(stm);
    fol_t Elt = SElt->t;
    fkey_t Sk(Elt) = SElt->k;
    Deref_And_Fail_Unless(Elt,FOLSMBP(Elt));
    XML_Indent(stream);
    Stream_Puts("</",stream);
    XML_atom_print_aux(stream,Elt);
    Stream_Putc('>',stream);
    Succeed;
}


char *
UTF8_to_Latin1(char * utf8) 
{
    unsigned int l = 1+strlen(utf8);
    char * latin1 = (char *) GC_MALLOC_ATOMIC(l);
    int in = l;
    int out = l;
    UTF8Toisolat1(latin1,&out,utf8,&in);
    return latin1;
}


char *
UTF8_to_XMLLatin1(char * utf8) 
{
    unsigned int l = 1+strlen(utf8);
    char * latin1 = (char *) GC_MALLOC_ATOMIC(l);
    int in = l;
    int out = l;
    char * xlatin1 = (char *) GC_MALLOC_ATOMIC(6*l);
    char * p = xlatin1;
    UTF8Toisolat1(latin1,&out,utf8,&in);
    for(;*latin1 ; latin1++,p++) {
        switch (*latin1) {
            case '&':
                sprintf(p,"&amp;");
                p += 4;
                break;
            case '<':
                sprintf(p,"&lt;");
                p += 3;
                break;
            case '>':
                sprintf(p,"&gt;");
                p += 3;
                break;
            default:
                *p = *latin1;
                break;
        }
    }
    *p=0;
    return xlatin1;
}




/**
 * XML_ConvertInput:
 * @in: string in a given encoding
 * @encoding: the encoding used
 *
 * Converts @in into UTF-8 for processing with libxml2 APIs
 *
 * Returns the converted UTF-8 string, or NULL in case of error.
 */
xmlChar *
XML_ConvertInput(const char *in, const char *encoding)
{
    xmlChar *out;
    int ret;
    int size;
    int out_size;
    int temp;
    xmlCharEncodingHandlerPtr handler;

    if (in == 0)
        return 0;

    handler = xmlFindCharEncodingHandler(encoding);

    if (!handler) {
        printf("ConvertInput: no encoding handler found for '%s'\n",
               encoding ? encoding : "");
        return 0;
    }

    size = (int) strlen(in) + 1;
    out_size = size * 2 - 1;
    out = (unsigned char *) xmlMalloc((size_t) out_size);

    if (out != 0) {
        temp = size - 1;
        ret = handler->input(out, &out_size, (const xmlChar *) in, &temp);
        if ((ret < 0) || (temp - size + 1)) {
            if (ret < 0) {
                printf("ConvertInput: conversion wasn't successful.\n");
            } else {
                printf
                    ("ConvertInput: conversion wasn't successful. converted: %i octets.\n",
                     temp);
            }

            xmlFree(out);
            out = 0;
        } else {
            out = (unsigned char *) xmlRealloc(out, out_size + 1);
            out[out_size] = 0;  /*null terminating out */
        }
    } else {
        printf("ConvertInput: no mem\n");
    }

    return out;
}


Bool
XML_textWriter_start_element( xmlTextWriterPtr writer, const char * encoding, sfol_t SElt, sfol_t SAttr, Bool empty)
{
    fol_t Elt = SElt->t;
    fkey_t Sk(Elt) = SElt->k;
    fol_t Attr = SAttr->t;
    fkey_t Sk(Attr) = SAttr->k;
    Deref_And_Fail_Unless(Elt,FOLSMBP(Elt));
    xmlTextWriterStartElement(writer,BAD_CAST FOLSMB_NAME(Elt));
    Deref(Attr);
    for(;FOLPAIRP(Attr);) {
        fol_t A=FOLPAIR_CAR(Attr);
        fkey_t Sk(A) = Sk(Attr);
        xmlChar *tmpX;
        xmlChar *tmpV;
        Deref(A);
        if (!FOLCMPP(A) && FOLCMP_ARITY(A) != 2)
            Fail;
        else {
            fol_t X=FOLCMP_REF(A,1);
            fkey_t Sk(X) = Sk(A);
            fol_t V=FOLCMP_REF(A,2);
            fkey_t Sk(V) = Sk(A);
            Deref(X);
            Deref(V);
            if (!FOLSMBP(X) || !FOLSMBP(V))
                Fail;
            tmpX=XML_ConvertInput(FOLSMB_NAME(X),encoding);
            tmpV=XML_ConvertInput(FOLSMB_NAME(V),encoding);
            xmlTextWriterWriteAttribute(writer,tmpX,tmpV);
        }
        Attr = FOLPAIR_CDR(Attr);
        Deref(Attr);
    }
    if (empty)
        xmlTextWriterEndElement(writer);
//    dyalog_printf("Done element\n");
    Succeed;
}

Bool
XML_textWriter_close_element( xmlTextWriterPtr writer, sfol_t SElt)
{
    fol_t Elt = SElt->t;
    xmlTextWriterEndElement(writer);
    Succeed;
}

Bool
XML_textWriter_string( xmlTextWriterPtr writer, const char *encoding, const char *s) 
{
    xmlChar *tmp = XML_ConvertInput(s,encoding);
    xmlTextWriterWriteString(writer,s);
    Succeed;
}

fol_t
DyALog_xmlTextReaderValue(xmlTextReaderPtr reader) 
{
    char *s = (char *)xmlTextReaderValue(reader);
    fol_t f = find_folsmb(s,0);
    xmlFree(s);
    return f;
}

fol_t
DyALog_xmlTextReaderPrefix(xmlTextReaderPtr reader) 
{
    char *s = (char *)xmlTextReaderPrefix(reader);
    fol_t f = find_folsmb(s,0);
    xmlFree(s);
    return f;
}

fol_t
DyALog_xmlTextReaderName(xmlTextReaderPtr reader) 
{
    char *s = (char *)xmlTextReaderName(reader);
    fol_t f = find_folsmb(s,0);
    xmlFree(s);
    return f;
}

fol_t
DyALog_xmlTextReaderLocalName(xmlTextReaderPtr reader) 
{
    char *s = (char *)xmlTextReaderLocalName(reader);
    fol_t f = find_folsmb(s,0);
    xmlFree(s);
    return f;
}

typedef fol_t (*cfun_t) (char *);

fol_t
DyALog_xmlAttributeCFun(xmlTextReaderPtr reader, cfun_t fun) 
{
    char *s = (char *)xmlTextReaderValue(reader);
    fol_t res = (*fun)(s);
    xmlFree(s);
    return res;
}


                        
