/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2005, 2009, 2010, 2011, 2014 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  libdyalogxml.pl -- DyALog library for parsing and writing xml
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % Interface
 
:-module(xml).

:-import{ file=>'format.pl', preds => [format/2,format_hook/4,format/3] }.

:-std_prolog
	reader/2,
	stream_reader/3,
	read_raw_event/2,
	read_event/3,
	node!type/2,
	node!depth/2,
	node!localname/2,
	node!name/2,
	node!prefix/2,
	node!full_name/2,
	node!value/2,
	node!inner_value/2,
	node!next_attribute/1,
	node!empty/1,
	event_ctx/2,
	event_process/3
	.

%% Callbacks for attributes

:-light_tabular
	attribute_handler/3
	.

:-mode(attribute_handler/3,+(+,+,-)).

:-rec_prolog
	attribute_dyalog_handler/4
	.



%% For output

:-std_prolog stream_writer/3,
	stream_writer_emit/1
	.

:-std_prolog
	node!name/2.

:-rec_prolog
	event_handler/5,
	event_process/4.

%% May be used to apply default handler
:-light_tabular
	event_super_handler/2
	.

%% No private namespace for features (not in module 'xml')
%% but exported to importing files
:-features(start_element,[name,attributes,empty]).
:-features(element,[name,attributes]).
:-features(end_element,[name]).
:-features([characters,comment,cdata],[value]).
:-features([pi,entity],[name,value]).
:-features([dtd],[name,ext_id,system_id]).

:-features(writer,[buffer,encoding,stream,writer]).

:-end_require.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation

reader(Filename,Reader) :-
	'$interface'('xmlNewTextReaderFilename'(Filename:string),
		    [return(Reader:ptr)])
	.

stream_reader(In,Reader,Encoding) :-
	(   Encoding = none xor true),
	xml!map!encoding(Encoding,EncodingN),
	'$interface'('xmlParserInputBufferCreateFd'(In:input,EncodingN:int),
		    [return(Buffer:ptr)]), 
	'$interface'('xmlNewTextReader'(Buffer:ptr,0:ptr),
		     [return(Reader:ptr)])
	/*%% Could be an alternate solution
	'$interface'('xmlReaderForFd'(In:input,0:ptr,Encoding:string,0:int),
		    [return(Reader:ptr)])
	*/
	.

read_raw_event(Reader,Status) :-
	'$interface'('xmlTextReaderRead'(Reader:ptr),
		    [return(Status:int)])
	.

node!type(Reader,Type) :-
	'$interface'('xmlTextReaderNodeType'(Reader:ptr),
		    [return(Type:int)])
	.

:-xcompiler
xml!inline_node!type(Reader,Type) :-
	'$interface'('xmlTextReaderNodeType'(Reader:ptr),
		    [return(Type:int)])
	.

node!depth(Reader,Depth) :-
	'$interface'('xmlTextReaderDepth'(Reader:ptr),
		    [return(Depth:int)])
	.

:-xcompiler
xml!inline_node!depth(Reader,Depth) :-
	'$interface'('xmlTextReaderDepth'(Reader:ptr),
		     [return(Depth:int)])
	.

node!localname(Reader,Name) :-
	'$interface'('DyALog_xmlTextReaderLocalName'(Reader:ptr),
		    [return(Name:term)])
	.

:-xcompiler
xml!inline_node!localname(Reader,Name) :-
	'$interface'('DyALog_xmlTextReaderLocalName'(Reader:ptr),
		    [return(Name:term)])
	.

node!name(Reader,Name) :-
	'$interface'('DyALog_xmlTextReaderName'(Reader:ptr),
		    [return(Name:term)])
	.

:-xcompiler
xml!inline_node!name(Reader,Name) :-
	'$interface'('DyALog_xmlTextReaderName'(Reader:ptr),
		    [return(Name:term)])
	.


node!prefix(Reader,Name) :-
	'$interface'('xmlTextReaderPrefix'(Reader:ptr),
		    [return(Name:string)])
	.

:-xcompiler
xml!inline_node!prefix(Reader,Name) :-
	'$interface'('xmlTextReaderPrefix'(Reader:ptr),
		     [return(Name:string)])
	.

node!value(Reader,Value) :-
	'$interface'('DyALog_xmlTextReaderValue'(Reader:ptr),
		    [return(Value:term)])
	.

:-xcompiler
xml!inline_node!value(Reader,Value) :-
	'$interface'('DyALog_xmlTextReaderValue'(Reader:ptr),
		    [return(Value:term)])
	.

node!inner_value(Reader,Value) :-
	'$interface'('DyALog_xmlTextReaderValue'(Reader:ptr),
		    [return(Value:term)])
	.

:-xcompiler
xml!inline_node!inner_value(Reader,Value) :-
	'$interface'('DyALog_xmlTextReaderValue'(Reader:ptr),
		    [return(Value:term)])
	.


node!empty(Reader) :-
	'$interface'('xmlTextReaderIsEmptyElement'(Reader:ptr),
		    [return(true:bool)])
	.

:-xcompiler
xml!inline_node!empty(Reader) :-
	'$interface'('xmlTextReaderIsEmptyElement'(Reader:ptr),
		    [return(true:bool)])
	.

node!next_attribute(Reader) :-
	'$interface'('xmlTextReaderMoveToNextAttribute'(Reader:ptr),
		    [return(R:int)]),
	R \== 0
	.

:-xcompiler
xml!inline_node!next_attribute(Reader) :-
	'$interface'('xmlTextReaderMoveToNextAttribute'(Reader:ptr),
		    [return(R:int)]),
	R \== 0
	.

node!full_name(Reader,Name) :-
	inline_node!localname(Reader,Local),
	(  
	    inline_node!prefix(Reader,Prefix) ->
	    atom_module(Name,Prefix,Local)
	;   
	    Name = Local
	)
	.

:-xcompiler
xml!inline_node!full_name(Reader,Name) :-
	inline_node!localname(Reader,Local),
	(  
	    inline_node!prefix(Reader,Prefix) ->
	    atom_module(Name,Prefix,Local)
	;   
	    Name = Local
	)
	.

%% For writing:

stream_writer(Out,
	      Writer::writer{ buffer => Buffer,
			      encoding => Encoding,
			      stream=> Out,
			      writer => Ptr
			    },
	      Encoding) :-
	(   Encoding = 'ISO-8859-1' xor true),
	(   Out = [] xor true ),
	xml!map!encoding(Encoding,EncodingN),
%%	'$interface'('xmlSubstituteEntitiesDefault'(1:int),[return(_:int)]),
	'$interface'('xmlGetCharEncodingHandler'(EncodingN:int),[return(EncHandler:ptr)]),
	format('HERE: ~w ~w\n',[Writer,EncodingN]),
	'$interface'('xmlOutputBufferCreateFd'(Out:output,EncHandler:ptr),
		     [return(Buffer:ptr)]),
	format('buffer: ~w ~w\n',[Buffer,Ptr]),
	'$interface'('xmlNewTextWriter'(Buffer:ptr,0:ptr),
		     [return(Ptr:ptr)]),
	format('writer: ~w ~w\n',[Buffer,Ptr]),
	'$interface'('xmlTextWriterSetIndent'(Ptr:ptr,1:int),[return(R:int)]), R==0,
	'$interface'('xmlTextWriterSetIndentString'(Ptr:ptr, '     ' : string),[return(R:int)]), R==0,
	true
	.

stream_writer_emit(Writer::writer{ buffer => Buffer,
				   stream=> Stream,
				   writer => Ptr,
				   encoding => Encoding
				 } ) :-
%%	format('I am here0 ~w\n',[Ptr]),
	%%	xmlDocContentDumpOutput(buf, cur, encoding, format)
%%	'$interface'('xmlDocContentDumpOutput'(Buffer:ptr,0:int,Encoding:string,1:int),[]),
	'$interface'( 'xmlFreeTextWriter'(Ptr:ptr), [] ),
%%	format('I am here ~w\n',[Ptr]),
%%	'$interface'( 'XML_print'(Stream:output,Buffer:ptr), []),
%%	format('I am here2\n',[]),
%%	'$interface'( 'xmlBufferFree'(Buffer:ptr), [] ),
%%	'$interface'( 'xmlOutputBufferClose'(Buffer:ptr), [] ),
%%	format('I am here3\n',[]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Event mapping

%% Warning: extensional preds are not subject to automatic module shifting into xml
%% Shoud do it explicitily
:-extensional xml!map!type/2.

xml!map!type(1,start_element).
xml!map!type(15,end_element).
xml!map!type(2,attribute).
xml!map!type(3,characters).
xml!map!type(4,cdata).
xml!map!type(5,entity).
xml!map!type(6,entity_decl).
xml!map!type(7,processing_instruction).
xml!map!type(8,comment).
xml!map!type(9,document_node).
xml!map!type(10,dtd).
xml!map!type(11,document_fragment).
xml!map!type(12,notation).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Encoding mapping

:-extensional xml!map!encoding/2.

xml!map!encoding(none,0).
xml!map!encoding(utf8,1).
xml!map!encoding(utf16_le,2).
xml!map!encoding(utf16_be,3).
xml!map!encoding(ucs4_le,4).
xml!map!encoding(ucs4_be,5).
xml!map!encoding(ebcdic,6).
xml!map!encoding(ucs4_2143,7).
xml!map!encoding(ucs4_3412,8).
xml!map!encoding(ucs2,9).

xml!map!encoding('ISO-8859-1',10).
xml!map!encoding(iso_8859_1,10).
xml!map!encoding(latin1,10).

xml!map!encoding('ISO-8859-2',11).
xml!map!encoding(iso_8859_2,11).
xml!map!encoding(latin2,11).

xml!map!encoding(iso_8859_3,12).
xml!map!encoding(latin3,12).

xml!map!encoding(iso_8859_4,13).
xml!map!encoding(latin4,13).

xml!map!encoding(iso_8859_5,14).
xml!map!encoding(latin5,14).

xml!map!encoding(iso_8859_6,15).
xml!map!encoding(latin6,15).

xml!map!encoding(iso_8859_7,16).
xml!map!encoding(latin7,16).

xml!map!encoding(iso_8859_8,17).
xml!map!encoding(latin8,17).

xml!map!encoding(iso_8859_9,18).
xml!map!encoding(latin9,18).

xml!map!encoding('2002_jp',19).
xml!map!encoding(shift_jp,20).
xml!map!encoding(euc_jp,21).
xml!map!encoding(ascii,22).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Event builders

read_event(Reader,Event,Depth) :-
	read_raw_event(Reader,Status),
	(   Status == 1 ->
	    inline_node!depth(Reader,Depth),
	    inline_node!type(Reader,Type),
	    fill_event(Type,Reader,Event)
	;   %% Should set error handling
	    Event = eof,
	    Depth = 0
	)
	.

:-xcompiler	
inlined_read_event(Reader,Event) :-
	'$interface'('xmlTextReaderRead'(Reader:ptr),[return(Status:int)]),
	(   Status == 1 ->
%%	    inline_node!depth(Reader,Depth),
	    inline_node!type(Reader,Type),
	    fill_event(Type,Reader,Event)
	;   %% Should set error handling
	    Event = eof,
	    Depth = 0
	)
	.


:-rec_prolog fill_event/3.

fill_event(1,Reader,
	   start_element{ name=>Name,
			  attributes=>Attributes,
			  empty => Empty
			}) :-
	inline_node!full_name(Reader,Name),
	(inline_node!empty(Reader) -> Empty = true ; Empty = false),
	build_attributes(Reader,Attributes,Name),
	true
	.

:-std_prolog build_attributes/3.

build_attributes(Reader,Attributes,Elt) :-
	(   inline_node!next_attribute(Reader) ->
%	    fill_event(2,Reader,Attr),
	    inline_node!full_name(Reader,Name),
	    ( attribute_handler(Elt,Name,Handler) ->
	      ( Handler = cfun(CFunPtr) ->
		'$interface'('DyALog_xmlAttributeCFun'(Reader:ptr,CFunPtr:ptr), [return(Value:term)]),
		Attributes = [Name:Value|Attributes2]
	      ; Handler = dyalog ->
		inline_node!inner_value(Reader,_Value),
		attribute_dyalog_handler(Elt,Name,_Value,Value),
		Attributes = [Name:Value|Attributes2]
	      ; Handler = skip,
		Attributes = Attributes2
	      )
	    ;
	      inline_node!inner_value(Reader,Value),
	      Attributes = [Name:Value|Attributes2]
	    ),
%%	    format('attribute handling elt=~w name=~w value=~w\n',[Elt,Name,Value]),
	    build_attributes(Reader,Attributes2,Elt)
	;
	    Attributes = [],
%%	    '$interface'('xmlTextReaderMoveToElement'(Reader:ptr),[]),
	    true
	)
	.

fill_event(2,Reader,Name:Value) :-
	inline_node!full_name(Reader,Name),
	inline_node!inner_value(Reader,Value)
	.

fill_event(15,Reader,
	   end_element{name=>Name}) :-
	inline_node!full_name(Reader,Name)
	.

fill_event(3,Reader,characters{value=>Value}) :-
	inline_node!value(Reader,Value)
	.

fill_event(4,Reader,cdata{value=>Value}) :-
	inline_node!value(Reader,Value)
	.

fill_event(5,Reader,entity) :-
	format('Get event\n',[])
	.

fill_event(6,Reader,entity_decl).

fill_event(7,Reader,pi{name=>Name,value=>Value}) :-
	inline_node!full_name(Reader,Name),
	inline_node!value(Reader,Value)
	.

fill_event(8,Reader,comment{value=>Value}) :-
	inline_node!value(Reader,Value)
	.

fill_event(9,Reader,document_node).
fill_event(10,Reader,dtd).
fill_event(11,Reader,document_fragment).
fill_event(12,Reader,notation).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% High Level Access

%% event_handler(Reader,In_Event,Out_Event,In_Ctx,Out_Ctx)

%% use loop as an event to start looping until finding some ending event

event_handler(Reader,loop,Out_Event,In_Ctx,Out_Ctx) :-
	(   Out_Event = eof xor true ),	%% unless specificied, loop until end of document
	mutable(M_Ctx,In_Ctx,true),
	repeat((
		inlined_read_event(Reader,_Event),
		mutable_read(M_Ctx,_Ctx),
		(   event_handler(Reader,_Event,_Event2,_Ctx,_Ctx2) ->
		    mutable(M_Ctx,_Ctx2),
		    _Event2 = Out_Event % Stop looping if current event equals stopping event
		;
		    _Event=Out_Event
		)
	       )),
	mutable_read(M_Ctx,Out_Ctx)
	.
	
%% other handlers are user defined on events generated by xml parser and user defined event (such as loop)
%% Look at test.pl for a complete and complex example

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

event_ctx(Ctx,Init) :-
	mutable(Ctx,Init)
	.

event_process(Handler,Event,Ctx) :-
	( Event = [_Event|Events2] ->
%%	    format('Event process: handler=~w Event=~w\n',[Handler,_Event]),
	    event_process(Handler,_Event,Ctx),
%%	    format('Event processed: handler=~w Event=~w\n',[Handler,_Event]),
	    event_process(Handler,Events2,Ctx)
%%	    every((domain(_Event,Event), event_process(Handler,_Event,Ctx)))
	;   Event == [] ->
	    true
	;   
	    mutable_read(Ctx,Ctx1),
	    (	( event_process(Handler,Event,Ctx1,Ctx2)
		xor event_super_handler(Handler,Super_Handler),
		    event_process(Super_Handler,Event,Ctx1,Ctx2)),
		mutable(Ctx,Ctx2)
	    xor true		% 'do nothing' as Default handler
	    )
	)
	.

%% Default handler for printing indented xml
%% We could have used only format
%% but we added specialized C functions for better efficiency
%% Should also be the way to correctly handle escaping and encoding
%% by using libmxl API
%% *** Still to be done ***

event_process(default(Stream),
	      dtd{name=>Name,
		  ext_id=>Ext_Id,
		  system_id=>Sys_Id},
	      0,0) :-
	format('<!DOCTYPE ~w PUBLIC "~w"\n\tSYSTEM "~w">',[Name,Ext_Id,Sys_Id]).

event_process(default(Stream),start_document,N,0).

event_process(default(Stream),end_document,N,N) :-
	format(Stream,'\n',[]).

event_process(default(Stream),characters{value=>Text},N,N) :-
	format(Stream,'~I~T',[Text]).

event_process(default(Stream),comment{value=>Text},N,N) :-
	format(Stream,'~I<!--~w-->',[Text]).

event_process(default(Stream),cdata{value=>Text},N,N) :-
	format(Stream,'\n[CDATA[~w]]',[Text]).

event_process(default(Stream),pi{name=>Name,value=>Value},N,N) :-
	( Value=[_|_] ->
	    format(Stream,'~I<?~w~L?>',[Name,[' ~A',' '],Value])
	;   
	    format(Stream,'~I<?~w ~w?>',[Name,Value])
	).

event_process(default(Stream),entity{name=>Name,value=>Text},N,N) :-
	format(Stream,'&~w;',[Name]).

event_process(default(Stream),element{name=>Name,attributes=>Attr},N,N) :-
	(   Attr = [] xor true),
	'$interface'('XML_start_element_print'(Stream:output,Name:term,Attr:term,true:bool),[])
	.

event_process(default(Stream),start_element{name=>Name,attributes=>Attr,empty=>Empty},N,M) :-
	(   Attr = [] xor true),
	(   Empty = false xor true),
	'$interface'('XML_start_element_print'(Stream:output,Name:term,Attr:term,Empty:bool),[]),
	( Empty == true ->
	    N=M
	;   
	    M is N+1,
	    private!set_indent(M)
	)
	.

event_process(default(Stream),end_element{name=>Name},N,M) :-
	M is N-1,
	private!set_indent(M),
	'$interface'('XML_close_element_print'(Stream:output,Name:term),[]).
	
format_hook(0'L,Stream,[[Format,Sep],AA|R],R) :-
        mutable(M,0,true),
        every((   domain(A,AA),
                  mutable_inc(M,V),
                  (   V == 0 xor write(Stream,Sep) ),
                  format(Stream,Format,[A]) ))
        .

format_hook(0'A,Stream,[A:V|R],R) :-
%%        format(Stream,'~E="~w"',[A,V])
	'$interface'('XML_attribute_value_print'(Stream:output,A:term,V:term),[])
        .

format_hook(0'E,Stream,[E|R],R) :-
	'$interface'('XML_atom_print'(Stream:output,E:term),[])
	.

format_hook(0'T,Stream,[E|R],R) :-
	'$interface'('XML_string_print'(Stream:output,E:term),[])
	.

:-light_tabular private!expanded_name/2.

private!expanded_name(E,XE) :-
	atom_module(E,M,EE),
	( M == [] ->
	    XE=E
	;   
	    private!expanded_name(EE,XEE),
	    private!name_builder('~w:~w',[XEE,EE],XE)
	)
	.

:-xcompiler((
             xml!private!newline_start(Start) :- '$interface'( 'Newline_Start_1'(Start:term),[])
            ))
.


:-std_prolog private!name_builder/3.

private!name_builder(Format,Args,Name) :-
        string_stream(_,S),
        format(S,Format,Args),
        flush_string_stream(S,Name),
        close(S)
        .


format_hook(0'I,Stream,R,R) :-
        private!newline_start(Start),
        format('\n~w',[Start])
        .


:-light_tabular private!indenter/2.

private!indenter(0,'').
private!indenter(N,Indent) :-
	N > 0,
	M is N-1,
	private!indenter(M,Indent2),
	atom_concat(Indent2,'  ',Indent)
	.

:-std_prolog private!set_indent/1.

private!set_indent(N) :-
	private!indenter(N,Indent),
	private!newline_start(Indent)
	.

	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Default handler for printing indented xml
%% We could have used only format
%% but we added specialized C functions for better efficiency
%% Should also be the way to correctly handle escaping and encoding
%% by using libmxl API
%% *** Still to be done ***

event_process(Writer::writer{ stream=> Stream, writer=>Ptr, encoding=>Encoding },start_document,N,0) :-
	stream_writer(Stream,Writer,Encoding),
%%	format('Created document ~w\n',[Writer]),
%%	'$interface'('xmlTextWriterStartDocument'(Ptr:ptr,0:ptr,Encoding: string,0:ptr),[return(R:int)]),
	'$interface'('xmlTextWriterStartDocument'(Ptr:ptr,0:ptr,0:ptr,0:ptr),[return(R:int)]),
	R \== -1,
%%	format('Started document ~w\n',[Writer]),
	true
	.

event_process(Writer::writer{ writer => Ptr},end_document,N,N) :-
%%	format('Closing: ~w\n',[Ptr]),
	'$interface'('xmlTextWriterEndDocument'(Ptr:ptr),[return(R:int)]),
	R \== -1,
%%	format('Emitting: ~w\n',[Ptr]),
	stream_writer_emit(Writer).

event_process(Writer::writer{ writer => Ptr},
	      dtd{name=>Name,
		  ext_id=>Ext_Id,
		  system_id=>Sys_Id},
	      0,0) :-
	'$interface'('xmlTextWriterStartDTD'(Ptr:ptr,Name:string,Ext_Id:string,Sys_Id:string),[return(_:int)]),
	'$interface'('xmlTextWriterEndDTD'(Ptr:ptr),[return(_:int)])
	.

event_process(Writer::writer{ stream => Stream, writer=> Ptr, encoding => Encoding },
	      characters{value=>Text},N,N) :-
	'$interface'('XML_textWriter_string'(Ptr:ptr,Encoding:string,Text:string),[])
	.

event_process(Writer::writer{ stream => Stream },comment{value=>Text},N,N) :-
	format(Stream,'~I<!--~w-->',[Text]).

event_process(Writer::writer{ stream => Stream },cdata{value=>Text},N,N) :-
	format(Stream,'\n[CDATA[~w]]',[Text]).

event_process(Writer::writer{ stream => Stream },pi{name=>Name,value=>Value},N,N) :-
	( Value=[_|_] ->
	    format(Stream,'~I<?~w~L?>',[Name,[' ~A',' '],Value])
	;   
	    format(Stream,'~I<?~w ~w?>',[Name,Value])
	).

event_process(Writer::writer{ stream => Stream },entity{name=>Name,value=>Text},N,N) :-
	format(Stream,'&~w;',[Name]).

event_process(Writer::writer{ stream => Stream, writer=> Ptr, encoding=> Encoding },
	      element{name=>Name,attributes=>Attr},N,N) :-
%%	format('Element: ~w\n',[Ptr]),
	(   Attr = [] xor true),
	'$interface'('XML_textWriter_start_element'(Ptr:ptr,Encoding:string,Name:term,Attr:term,true:bool),[])
	.

event_process(Writer::writer{ stream => Stream, writer=> Ptr, encoding=> Encoding },
	      start_element{name=>Name,attributes=>Attr,empty=>Empty},N,N) :-
	(   Attr = [] xor true),
	(   Empty = false xor true),
	'$interface'('XML_textWriter_start_element'(Ptr:ptr,Encoding:string,Name:term,Attr:term,Empty:bool),[])
	.

event_process(Writer::writer{ stream => Stream, writer => Ptr },end_element{name=>Name},N,N) :-
	'$interface'('XML_textWriter_close_element'(Ptr:ptr,Name:term),[]).
