/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2005 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  test.pl -- Small test file for libdyalogxml
 *
 * ----------------------------------------------------------------
 * Description
 *   A small sample file to test the libxml2 API in DyALog
 *   and the design of XML event_handlers
 * ----------------------------------------------------------------
 */

 :-import{ module=> xml, 
	   file => 'libdyalogxml.pl',
	   preds => [
		     %% Low level predicates
		     %%		     node!type/2,
		     %%		     node!depth/2,
		     %%		     node!localname/2,
		     %%		     node!name/2,
		     %%		     node!value/2,
		     %%		     node!prefix/2,
		     reader/2,
		     stream_reader/3,
		     read_event/3,
		     read_raw_event/2,
		     event_handler/5,
		     event_process/3,
		     event_process/4,
		     event_ctx/2,
		     event_super_handler/2
		     ]
	 }.


:-require('format.pl').

:-features(ctx,[class,node,constraint,current]).
:-finite_set(constraint,[provides,needs,father,precedes,equals,dominates]).
:-subset(res_const,constraint[provides,needs]).
:-subset(node_const,constraint[father,precedes,equals,dominates]).

%%?-argv([File]),reader(File,Reader),event_handler(Reader,loop,eof,[],[]).

?-stream_reader([],Reader,'latin1'),event_handler(Reader,loop,eof,[],[]).

%%?-emit_xml.

emit_xml :-
	event_ctx(Ctx,0),
	event_process(no_comment([]),
	      [ start_document,
		dtd{name=>toto,
		    ext_id=>'-//INRIA//DTD TAG 1.0//EN',
		    system_id=>'http://alpage.inria.fr/~clerger/tag.dtd,xml'
		   },
		pi{name=>xml,value=> [version:'1.0',encoding:'latin1']},
		start_element{name=>foo,attributes=>[foo!a:x,b:y]},
		[ element{name=>toto!bar},
		  characters{value=>'du text'},
		  comment{value=>'Un commentaire'},
		  start_element{name=>foo,attributes=>[a:x,b:y]},
		  [ element{name=>bar},
		    characters{value=>'du text'},
		    comment{value=>'Un commentaire'},
		    element{name=>bar2}
		  ],
		  end_element{name=>foo},
		  element{name=>bar2}
		],
		end_element{name=>foo},
		end_document
	      ],
	      Ctx
	     )
	.

event_super_handler(no_comment(Stream),default(Stream)).

event_process(no_comment(_),comment{},N,N).

?-emit_new_xml.

emit_new_xml :-
	event_ctx(Ctx,0),
	event_process(writer{ },
		      [ start_document,
			dtd{name=>toto,
			    ext_id=>'-//INRIA//DTD TAG 1.0//EN',
			    system_id=>'http://alpage.inria.fr/~clerger/tag.dtd,xml'
			   },
			start_element{ name => titi, attributes=> [alpha:�ric] },
			element{ name => toto, attributes=> [alpha:�ric] },
			characters{ value => 'Je suis ici <�h�!>' },
			end_element{ name => titi },
			end_document
		      ],
		      Ctx
		     )
	.

/*
?-stream_reader([],Reader,latin1),
event_ctx(Ctx,0),
repeat((
	read_event(Reader,Event,Depth),
	event_process(default([]),Event,Ctx),
%%	event_process(debug,Event,Ctx),
	Event == eof
       )).
*/

event_process(debug,Event,Ctx,Ctx) :- format('~w\n',[Event]).

/*
?-stream_reader([],Reader,latin1),event_ctx(Ctx,0),loop(Reader,Ctx).

:-std_prolog loop/2.

loop(Reader,Ctx) :-
	(   read_event(Reader,Event,Depth),
	    event_process(default([]),Event,Ctx),
	    Event == eof
	xor loop(Reader,Ctx)
	)
	.
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event handler
%%    handle XML base event (start and end elements)
%%    but also send new events (for instance 'fs' at the end of a fs value)

%%    The use of In_Ctx and Out_Ctx may be quite complex due to the power of logic variables
%%    and the way one wish to build ctx

%% Roughly speaking, the two main alternatives are
%%     * start from an 'empty' ctx and get a structured output ctx
%%     * start from a variable ctx which is progressivly bound to larger structures,
%%     the output ctx beeing some base structure such as [] (see fs structures)

%% Ctx may be used to store tmp information from outside the current element and/or
%% build some structure using content nested in current element.

event_handler( Reader,
	       start_element{name=>E::class,attributes=>[name:Class]},
	       OE::end_element{name=>E},
	       [],
	       []
	     ):-
	format('\n:-class(~q).\n',[Class]),
	event_handler(Reader,loop,OE,ctx{class=>Class},_)
	.

event_handler( Reader,
	       OE::start_element{name=>s,attributes=>[name:Super]},
	       OE,
	       Ctx::ctx{class=>Class},
	       Ctx
	     ) :-
	format(':-super(~q,~q).\n',[Class,Super])
	.

event_handler( Reader,
	       OE::start_element{name=>C::res_const[]},
	       OE,
	       ctx{class=>Class},
	       ctx{class=>Class,constraint=>C}
	     ).

event_handler( Reader,
	       OE::start_element{name=>pf,attributes=>[name:Res]},
	       OE,
	       ctx{class=>Class,constraint=>C},
	       ctx{class=>Class}
	     ) :-
	format(':-~w(~q,~q).\n',[C,Class,Res])
	.

event_handler( Reader,
	       start_element{name=>E::node,attributes=>[name:Node]},
	       OE::end_element{name=>E},
	       Ctx::ctx{class=>Class},
	       Ctx
	     ) :-
	format(':-node(~q,~q).\n',[Class,Node]),
	event_handler(Reader,loop,OE,[],FS),
	(   var(FS) xor format(':-nodefeature(~q,~q,~q).\n',[Class,Node,FS]))
	.

event_handler( Reader,
	       OE::start_element{name=>C::node_const,
				 attributes=>Attr,
				 empty=>true
				},
	       OE,
	       Ctx::ctx{class=>Class},
	       Ctx
	     ) :-
	domain(arg1:Arg1,Attr),
	domain(arg2:Arg2,Attr),
	format(':-~w(~q,~q,~q).\n',[C,Class,Arg1,Arg2])
	.

event_handler( Reader,
	       start_element{name=>E::description,empty=>false},
	       OE::end_element{name=>E},
	       Ctx::ctx{class=>Class},
	       Ctx
	     ) :-
	event_handler(Reader,loop,OE,[],V),
	format(':-description(~w,~w).\n',[Class,V])
	.

event_handler( Reader,
	       start_element{name=>fs,empty=>true},
	       fs,
	       V1,
	       V2
	     ) :-
	value_extend(V1,[],V2)
	.

event_handler( Reader,
	       start_element{name=>fs,empty=>false},
	       OE::fs,
	       V1,
	       V2
	     ) :-
	event_handler(Reader,loop,OE,FS,[]),
	value_extend(V1,FS,V2)
	.

event_handler( Reader,
	       end_element{name=>fs},
	       fs,
	       [],
	       []
	     ).

event_handler( Reader,
	       start_element{name=>E::f,attributes=>[name:Feature]},
	       OE::end_element{name=>E},
	       [Feature:V|FS2],
	       FS2
	     ) :-
	event_handler(Reader,
		      loop,
		      OE,
		      [],
		      V)
	.

event_handler( Reader,
	       OE::start_element{name=>sym,attributes=>[value:V]},
	       fs,
	       V1,
	       V2
	     ) :-
	value_extend(V1,V,V2).

event_handler( Reader,
	       start_element{name=>E::vAlt},
	       fs,
	       V1,
	       V2
	     ) :-
	event_handler( Reader,loop,end_element{name=>E},disj(L),disj([])),
	value_extend(V1,disj(L),V2)
	.

event_handler( Reader,
	       start_element{name=>E::var,attributes=>[name:Var],empty=>Empty},
	       fs,
	       V1,
	       V3
	     ) :-	
	value_extend(V1,var(Var),V2),
	( Empty == true ->
	    V3=V2
	;   
	    event_handler(Reader,loop,end_element{name=>E},[],V),
	    value_extend(V2,V,V3)
	)
	.

event_handler( Reader,
	       start_element{name=>E::binding,empty=>false},
	       OE::end_element{name=>E},
	       Ctx::ctx{class=>Class},
	       Ctx
	     ) :-
	event_handler(Reader,loop,fs,[],V1),
	event_handler(Reader,loop,OE,[],V2),
	format(':-binding(~q,~q,~q).\n',[Class,V1,V2])
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:-std_prolog value_extend/3.

value_extend(V1,V2,V3) :-
	( V1 == [] ->
	    V3=V2
	;   V1 =  disj([V2|L]) ->
	    V3 = disj(L)
	;   
	    V3 = V1 ^ V2
	)
	.
